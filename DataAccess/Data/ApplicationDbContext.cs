﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Restauracja_u_Domino.Models;

namespace Restauracja_u_Domino.DataAccess.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Table>().HasData(
                new Table { Id = 1, SeatsNumb = 4, Description = "przy oknie, blisko szatni" },
                new Table { Id = 2, SeatsNumb = 4, Description = "przy ścianie" },
                new Table { Id = 3, SeatsNumb = 4, Description = "przy oknie" },
                new Table { Id = 4, SeatsNumb = 2, Description = "przy oknie" },
                new Table { Id = 5, SeatsNumb = 2, Description = "przy oknie" },

                new Table { Id = 6, SeatsNumb = 4, Description = "przy ścianie" },
                new Table { Id = 7, SeatsNumb = 2, Description = "przy ścianie, blisko schodów" },
                new Table { Id = 8, SeatsNumb = 4, Description = "przy ścianie, blisko WC" },
                new Table { Id = 9, SeatsNumb = 8, Description = "przy ścianie, okrągły, duży" },
                new Table { Id = 10, SeatsNumb = 4, Description = "przy ścianie, blisko schodów" },

                new Table { Id = 11, SeatsNumb = 2, Description = "przy ścianie" },
                new Table { Id = 12, SeatsNumb = 4, Description = "przy ścianie, na tarasie" },
                new Table { Id = 13, SeatsNumb = 4, Description = "na tarasie" },
                new Table { Id = 14, SeatsNumb = 2, Description = "przy ścianie, na tarasie" },
                new Table { Id = 15, SeatsNumb = 2, Description = "przy ścianie, na tarasie" }
            );

            builder.Entity<Category>().HasData(
                new Category { Id = 1, CategoryName = "FastFood" },
                new Category { Id = 2, CategoryName = "Obiady" },
                new Category { Id = 3, CategoryName = "Owoce" },
                new Category { Id = 4, CategoryName = "Napoje" }
            );

            builder.Entity<Dish>().HasData(
                new Dish { Id = 1, Price = 20, Name = "Pizza Hawajska", Description = "Uwielbiana przez wszystkich pizza w naszym specjalnym wydaniu (z samym ananasem)", ImageUrl = "", CategoryId = 1 },
                new Dish { Id = 2, Price = 15, Name = "Pizza Margherita", Description = "To że najtańsza nie znaczy, że najgorsza (ser na specjalne życzenie zamawiającego)", ImageUrl = "", CategoryId = 1 },
                new Dish { Id = 3, Price = 24, Name = "Pizza Funghi", Description = "Nie wiadomo z czym to ale brzmi ciekawie...", ImageUrl = "", CategoryId = 1 },
                new Dish { Id = 4, Price = 22, Name = "Pizza z Rybą", Description = "Mówi się, że pizza z rybą to lepsze niż ser dlatego nie dajemy do niej sera", ImageUrl = "", CategoryId = 1 },
                new Dish { Id = 5, Price = 25, Name = "Pizza Peperoni", Description = "Klasyczna peperoni bez salami, posypana sładką papryką z torebki", ImageUrl = "", CategoryId = 1 },
                new Dish { Id = 6, Price = 8, Name = "Frytki", Description = "Im wino starsze tym lepsze, jeśli to samo tyczy się oleju do smażenia to nie ma lepszych frytek od naszych", ImageUrl = "", CategoryId = 1 },
                new Dish { Id = 7, Price = 10, Name = "Hamburger", Description = "Może i kotlet jest zrobiony z parówek ale za to bułki są dość świeże", ImageUrl = "/images/dishes/6.jpg", CategoryId = 1 },
                new Dish { Id = 8, Price = 8, Name = "Hot-Dog", Description = "Prawie jak hamburger tylko inny kształt", ImageUrl = "/images/dishes/7.jpg", CategoryId = 1 },
                new Dish { Id = 9, Price = 2, Name = "Zapiekanka", Description = "Niska cena nie musi od razu oznaczać słabego jedzenia. W tym przypadku jednak oznacza. Od czego jest wyobraźnia...", ImageUrl = "/images/dishes/12.jpg", CategoryId = 1 },
                new Dish { Id = 10, Price = 10, Name = "Kebab", Description = "Najlepszy kebab z extra sosiwem. Tylko dla prawdziwych twardzieli", ImageUrl = "/images/dishes/17.jpg", CategoryId = 1 },
                new Dish { Id = 11, Price = 10, Name = "Falafel", Description = "Danie dla tych dla których kebab był za ostry. Kotlet nie jest z parówek tylko z grochu ", ImageUrl = "", CategoryId = 1 },

                new Dish { Id = 12, Price = 12, Name = "Ziemniaczana uczta", Description = "Nazwa mówi wszystko co trzeba wiedzieć o tym daniu. Może poza tym, że ziemniaków jest mało i są trochę niedobre", ImageUrl = "/images/dishes/2.jpg", CategoryId = 2 },
                new Dish { Id = 13, Price = 12, Name = "Flaczki", Description = "Kto by pomyślał, że to białe to nie kluski a żołądek, ci Chińczycy to mieli łeb na karku", ImageUrl = "", CategoryId = 2 },
                new Dish { Id = 14, Price = 7, Name = "Kiełbasa kosmitów", Description = "Przepis na to danie podobno został skradziony kosmitom. Już łatwiej uwierzyć w czary", ImageUrl = "/images/dishes/3.jpg", CategoryId = 2 },
                new Dish { Id = 15, Price = 6, Name = "Kartofelki", Description = "To samo co ziemniaczana uczta tylko ziemniaki nie są tłuczone (i czasami nieobrane)", ImageUrl = "", CategoryId = 2 },
                new Dish { Id = 16, Price = 11, Name = "Gołąbki", Description = "Nasze ptaki przed ubojem są karmione wysokiej jakości ryżem", ImageUrl = "", CategoryId = 2 },
                new Dish { Id = 17, Price = 110, Name = "Pieczona świnia", Description = "Prawdziwa mięsna uczta (jabłko nie jest częścią zestawu i trzeba za nie dopłacić lub przynieść swoje)", ImageUrl = "/images/dishes/15.jpg", CategoryId = 2 },

                new Dish { Id = 18, Price = 6, Name = "Mirabelki", Description = "Sprowadzane od profesjonalnego sadownika, który mówi o nich \"miękkie, słodkie i duże\" ", ImageUrl = "", CategoryId = 3 },
                new Dish { Id = 19, Price = 6, Name = "Wiśnie", Description = "Z naszych sadów. Pół zjadły szpaki, pół robaki, a ostatnia uratowana połowa ląduje w naszym menu", ImageUrl = "", CategoryId = 3 },

                new Dish { Id = 20, Price = 2, Name = "Woda z kranu", Description = "Krystaliczna woda (zanim przejdzie przez nasze rury), pełna minerałów (dzięki temu, że przechodzi przez nasze rury)", ImageUrl = "", CategoryId = 4 },
                new Dish { Id = 21, Price = 40, Name = "Woda z czajnika (nieprzegotowana)", Description = "Woda jak z kranu ale polewana z ekskluzywnego czajnika ", ImageUrl = "/images/dishes/9.jpg", CategoryId = 4 },
                new Dish { Id = 22, Price = 32, Name = "4 razy wóda 4 razy", Description = "Po co zamawiać po jednym kieliszku jak można od razu 16", ImageUrl = "/images/dishes/10.jpg", CategoryId = 4 },
                new Dish { Id = 23, Price = 64, Name = "4 razy wóda 4 razy podwójnie", Description = "Po co dwa razy zamawiać 16 kieliszków skoro można za jednym razem zamówić 32", ImageUrl = "/images/dishes/11.jpg", CategoryId = 4 },
                new Dish { Id = 24, Price = 7, Name = "Piwo", Description = "Brzydzimy się dolewaniem wody do piwa. My dolewamy piwa do wody i to sprawiedliwie - fifty fifty", ImageUrl = "/images/dishes/16.jpg", CategoryId = 4 }
               
            );
        }

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Dish> Dishes { get; set; }
        public DbSet<RecommendedDishes> RecommendedDishes { get; set; }
        public DbSet<Table> Tables { get; set; }

        public DbSet<Reservation> Reservations { get; set; }
    }
}