﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Restauracja_u_Domino.DataAccess.Migrations
{
    public partial class SeedTablesAndDishes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Reservations_AspNetUsers_UserId",
                table: "Reservations");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "Reservations",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "CategoryName" },
                values: new object[,]
                {
                    { 1, "FastFood" },
                    { 2, "Obiady" },
                    { 3, "Owoce" },
                    { 4, "Napoje" }
                });

            migrationBuilder.InsertData(
                table: "Tables",
                columns: new[] { "Id", "Description", "SeatsNumb" },
                values: new object[,]
                {
                    { 13, "na tarasie", 4 },
                    { 12, "przy ścianie, na tarasie", 4 },
                    { 11, "przy ścianie", 2 },
                    { 10, "przy ścianie, blisko schodów", 4 },
                    { 9, "przy ścianie, okrągły, duży", 8 },
                    { 8, "przy ścianie, blisko WC", 4 },
                    { 6, "przy ścianie", 4 },
                    { 14, "przy ścianie, na tarasie", 2 },
                    { 5, "przy oknie", 2 },
                    { 4, "przy oknie", 2 },
                    { 3, "przy oknie", 4 },
                    { 2, "przy ścianie", 4 },
                    { 1, "przy oknie, blisko szatni", 4 },
                    { 7, "przy ścianie, blisko schodów", 2 },
                    { 15, "przy ścianie, na tarasie", 2 }
                });

            migrationBuilder.InsertData(
                table: "Dishes",
                columns: new[] { "Id", "CategoryId", "Description", "ImageUrl", "Name", "Price" },
                values: new object[,]
                {
                    { 1, 1, "Uwielbiana przez wszystkich pizza w naszym specjalnym wydaniu (z samym ananasem)", "", "Pizza Hawajska", 20f },
                    { 22, 4, "Po co zamawiać po jednym kieliszku jak można od razu 16", "/images/dishes/10.jpg", "4 razy wóda 4 razy", 32f },
                    { 21, 4, "Woda jak z kranu ale polewana z ekskluzywnego czajnika ", "/images/dishes/9.jpg", "Woda z czajnika (nieprzegotowana)", 40f },
                    { 20, 4, "Krystaliczna woda (zanim przejdzie przez nasze rury), pełna minerałów (dzięki temu, że przechodzi przez nasze rury)", "", "Woda z kranu", 2f },
                    { 19, 3, "Z naszych sadów. Pół zjadły szpaki, pół robaki, a ostatnia uratowana połowa ląduje w naszym menu", "", "Wiśnie", 6f },
                    { 18, 3, "Sprowadzane od profesjonalnego sadownika, który mówi o nich \"miękkie, słodkie i duże\" ", "", "Mirabelki", 6f },
                    { 17, 2, "Prawdziwa mięsna uczta (jabłko nie jest częścią zestawu i trzeba za nie dopłacić lub przynieść swoje)", "/images/dishes/15.jpg", "Pieczona świnia", 110f },
                    { 16, 2, "Nasze ptaki przed ubojem są karmione wysokiej jakości ryżem", "", "Gołąbki", 11f },
                    { 15, 2, "To samo co ziemniaczana uczta tylko ziemniaki nie są tłuczone (i czasami nieobrane)", "", "Kartofelki", 6f },
                    { 14, 2, "Przepis na to danie podobno został skradziony kosmitom. Już łatwiej uwierzyć w czary", "/images/dishes/3.jpg", "Kiełbasa kosmitów", 7f },
                    { 13, 2, "Kto by pomyślał, że to białe to nie kluski a żołądek, ci Chińczycy to mieli łeb na karku", "", "Flaczki", 12f },
                    { 12, 2, "Nazwa mówi wszystko co trzeba wiedzieć o tym daniu. Może poza tym, że ziemniaków jest mało i są trochę niedobre", "/images/dishes/2.jpg", "Ziemniaczana uczta", 12f },
                    { 11, 1, "Danie dla tych dla których kebab był za ostry. Kotlet nie jest z parówek tylko z grochu ", "", "Falafel", 10f },
                    { 10, 1, "Najlepszy kebab z extra sosiwem. Tylko dla prawdziwych twardzieli", "/images/dishes/17.jpg", "Kebab", 10f },
                    { 9, 1, "Niska cena nie musi od razu oznaczać słabego jedzenia. W tym przypadku jednak oznacza. Od czego jest wyobraźnia...", "/images/dishes/12.jpg", "Zapiekanka", 2f },
                    { 8, 1, "Prawie jak hamburger tylko inny kształt", "/images/dishes/7.jpg", "Hot-Dog", 8f },
                    { 7, 1, "Może i kotlet jest zrobiony z parówek ale za to bułki są dość świeże", "/images/dishes/6.jpg", "Hamburger", 10f },
                    { 6, 1, "Im wino starsze tym lepsze, jeśli to samo tyczy się oleju do smażenia to nie ma lepszych frytek od naszych", "", "Frytki", 8f },
                    { 5, 1, "Klasyczna peperoni bez salami, posypana sładką papryką z torebki", "", "Pizza Peperoni", 25f },
                    { 4, 1, "Mówi się, że pizza z rybą to lepsze niż ser dlatego nie dajemy do niej sera", "", "Pizza z Rybą", 22f },
                    { 3, 1, "Nie wiadomo z czym to ale brzmi ciekawie...", "", "Pizza Funghi", 24f },
                    { 2, 1, "To że najtańsza nie znaczy, że najgorsza (ser na specjalne życzenie zamawiającego)", "", "Pizza Margherita", 15f },
                    { 23, 4, "Po co dwa razy zamawiać 16 kieliszków skoro można za jednym razem zamówić 32", "/images/dishes/11.jpg", "4 razy wóda 4 razy podwójnie", 64f },
                    { 24, 4, "Brzydzimy się dolewaniem wody do piwa. My dolewamy piwa do wody i to sprawiedliwie - fifty fifty", "/images/dishes/16.jpg", "Piwo", 7f }
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Reservations_AspNetUsers_UserId",
                table: "Reservations",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Reservations_AspNetUsers_UserId",
                table: "Reservations");

            migrationBuilder.DeleteData(
                table: "Dishes",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Dishes",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Dishes",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Dishes",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Dishes",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Dishes",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Dishes",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Dishes",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Dishes",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Dishes",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Dishes",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Dishes",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Dishes",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Dishes",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Dishes",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Dishes",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Dishes",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Dishes",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Dishes",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Dishes",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Dishes",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Dishes",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Dishes",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Dishes",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "Reservations",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddForeignKey(
                name: "FK_Reservations_AspNetUsers_UserId",
                table: "Reservations",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
