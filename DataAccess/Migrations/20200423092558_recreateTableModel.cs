﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Restauracja_u_Domino.DataAccess.Migrations
{
    public partial class recreateTableModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Placment",
                table: "Tables");

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Tables",
                maxLength: 50,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "Tables");

            migrationBuilder.AddColumn<string>(
                name: "Placment",
                table: "Tables",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }
    }
}
