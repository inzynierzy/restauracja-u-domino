﻿using Restauracja_u_Domino.Models;

namespace Restauracja_u_Domino.DataAccess.Repository.IRepository
{
    public interface IDishRepository : IRepository<Dish>
    {
        void Update(Dish dish);
    }
}