﻿using System;

namespace Restauracja_u_Domino.DataAccess.Repository.IRepository
{
    public interface IUnitOfWork : IDisposable
    {
        //Add here repository intefaces declaration
        //IModelRepository Model {get;}
        ICategoryRepository Category { get; }

        IDishRepository Dish { get; }

        IRecommendedDishesRepository RecommendedDishes { get; }

        ITableRepository Table { get; }
        
        void Save();
    }
}