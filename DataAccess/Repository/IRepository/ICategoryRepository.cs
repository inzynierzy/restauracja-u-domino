﻿using Restauracja_u_Domino.Models;

namespace Restauracja_u_Domino.DataAccess.Repository.IRepository
{
    public interface ICategoryRepository : IRepository<Category>
    {
        void Update(Category category);
    }
}