﻿using Restauracja_u_Domino.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Restauracja_u_Domino.DataAccess.Repository.IRepository
{
    public interface IRecommendedDishesRepository : IRepository<RecommendedDishes>
    {
        void Update(RecommendedDishes recommendedDish);
    }
}
