﻿using Restauracja_u_Domino.DataAccess.Data;
using Restauracja_u_Domino.DataAccess.Repository.IRepository;

namespace Restauracja_u_Domino.DataAccess.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _db;

        public UnitOfWork(ApplicationDbContext db)
        {
            _db = db;
            Category = new CategoryRepository(_db);
            Dish = new DishRepository(_db);
            RecommendedDishes = new RecommendedDishesRepository(_db);
            Table = new TableRepository(_db);
            //Add here repositories
            //Model = new ModelRepository(_db);
        }

        public ICategoryRepository Category { get; private set; }
        public IDishRepository Dish { get; private set; }

        public IRecommendedDishesRepository RecommendedDishes { get; private set; }

        public ITableRepository Table { get; private set; }

        //Add here repository interfaces
        //public IModelRepository Model {get; private set;}

        public void Dispose()
        {
            _db.Dispose();
        }

        public void Save()
        {
            _db.SaveChanges();
        }
    }
}