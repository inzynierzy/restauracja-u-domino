﻿using Restauracja_u_Domino.DataAccess.Data;
using Restauracja_u_Domino.DataAccess.Repository.IRepository;
using Restauracja_u_Domino.Models;
using System.Linq;

namespace Restauracja_u_Domino.DataAccess.Repository
{
    public class TableRepository : Repository<Table>, ITableRepository
    {
        private readonly ApplicationDbContext _db;

        public TableRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }


        public void Update(Table Table)
        {
            var objFromDb = _db.Tables.FirstOrDefault(t => t.Id == Table.Id);
            {
                objFromDb.SeatsNumb = Table.SeatsNumb;
                objFromDb.Description = Table.Description;

                _db.SaveChanges();
            }
        }
    }
}
