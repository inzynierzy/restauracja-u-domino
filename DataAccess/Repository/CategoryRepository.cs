﻿using Restauracja_u_Domino.DataAccess.Data;
using Restauracja_u_Domino.DataAccess.Repository.IRepository;
using Restauracja_u_Domino.Models;
using System.Linq;

namespace Restauracja_u_Domino.DataAccess.Repository
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        private readonly ApplicationDbContext _db;

        public CategoryRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        public void Update(Category category)
        {
            var objFromDb = _db.Categories.FirstOrDefault(s => s.Id == category.Id);
            if (objFromDb != null)
            {
                objFromDb.CategoryName = category.CategoryName;
                _db.SaveChanges();
            }
        }
    }
}