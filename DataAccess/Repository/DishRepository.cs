﻿using Restauracja_u_Domino.DataAccess.Data;
using Restauracja_u_Domino.DataAccess.Repository.IRepository;
using Restauracja_u_Domino.Models;
using System.Linq;

namespace Restauracja_u_Domino.DataAccess.Repository
{
    public class DishRepository : Repository<Dish>, IDishRepository
    {
        private readonly ApplicationDbContext _db;

        public DishRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        public void Update(Dish dish)
        {
            var objFromDb = _db.Dishes.FirstOrDefault(s => s.Id == dish.Id);
            if (objFromDb != null)
            {
                objFromDb.Description = dish.Description;
                objFromDb.Name = dish.Name;
                objFromDb.Price = dish.Price;
                objFromDb.ImageUrl = dish.ImageUrl;
                _db.SaveChanges();
            }
        }
    }
}