﻿using Restauracja_u_Domino.DataAccess.Data;
using Restauracja_u_Domino.DataAccess.Repository.IRepository;
using Restauracja_u_Domino.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Restauracja_u_Domino.DataAccess.Repository
{
    public class RecommendedDishesRepository : Repository<RecommendedDishes>, IRecommendedDishesRepository
    {
        private readonly ApplicationDbContext _db;

        public RecommendedDishesRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        public void Update(RecommendedDishes recommendedDish)
        {
            var objFromDb = _db.RecommendedDishes.FirstOrDefault(s => s.Id == recommendedDish.Id);
            if(objFromDb != null)
            {
                objFromDb.DishId = recommendedDish.DishId;
                _db.SaveChanges();
            }
        }
    }
}
