﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Restauracja_u_Domino.DataAccess.Data;
using Restauracja_u_Domino.Models;

[assembly: HostingStartup(typeof(Restauracja_u_Domino.Areas.Identity.IdentityHostingStartup))]
namespace Restauracja_u_Domino.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
            });
        }
    }
}