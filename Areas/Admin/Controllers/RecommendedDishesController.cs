﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Restauracja_u_Domino.DataAccess.Data;
using Restauracja_u_Domino.Models;

namespace Restauracja_u_Domino.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class RecommendedDishesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public RecommendedDishesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/RecommendedDishes
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.RecommendedDishes.Include(r => r.Dish);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Admin/RecommendedDishes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var recommendedDishes = await _context.RecommendedDishes
                .Include(r => r.Dish)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (recommendedDishes == null)
            {
                return NotFound();
            }

            return View(recommendedDishes);
        }

        // GET: Admin/RecommendedDishes/Create
        public IActionResult Create()
        {
            ViewData["DishId"] = new SelectList(_context.Dishes, "Id", "Name");
            return View();
        }

        // POST: Admin/RecommendedDishes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,DishId")] RecommendedDishes recommendedDishes)
        {
            if (ModelState.IsValid)
            {
                _context.Add(recommendedDishes);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["DishId"] = new SelectList(_context.Dishes, "Id", "Description", recommendedDishes.DishId);
            return View(recommendedDishes);
        }

        // GET: Admin/RecommendedDishes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var recommendedDishes = await _context.RecommendedDishes.FindAsync(id);
            if (recommendedDishes == null)
            {
                return NotFound();
            }
            ViewData["DishId"] = new SelectList(_context.Dishes, "Id", "Description", recommendedDishes.DishId);
            return View(recommendedDishes);
        }

        // POST: Admin/RecommendedDishes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,DishId")] RecommendedDishes recommendedDishes)
        {
            if (id != recommendedDishes.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(recommendedDishes);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RecommendedDishesExists(recommendedDishes.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["DishId"] = new SelectList(_context.Dishes, "Id", "Description", recommendedDishes.DishId);
            return View(recommendedDishes);
        }

        // GET: Admin/RecommendedDishes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var recommendedDishes = await _context.RecommendedDishes
                .Include(r => r.Dish)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (recommendedDishes == null)
            {
                return NotFound();
            }

            return View(recommendedDishes);
        }

        // POST: Admin/RecommendedDishes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var recommendedDishes = await _context.RecommendedDishes.FindAsync(id);
            _context.RecommendedDishes.Remove(recommendedDishes);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RecommendedDishesExists(int id)
        {
            return _context.RecommendedDishes.Any(e => e.Id == id);
        }
    }
}
