﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Restauracja_u_Domino.DataAccess.Data;
using Restauracja_u_Domino.Models;

namespace Restauracja_u_Domino.Areas.Employee.Controllers
{
    [Area("Employee")]
    [Authorize(Roles="Employee, Admin")]
    public class AllReservationController : Controller
    {
        private readonly ApplicationDbContext _db;

        public AllReservationController(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            List<Reservation> reservations = _db.Reservations.Include(r => r.User).Include(r => r.Table).Where(r => r.StartDate > DateTime.Now).ToList(); 
            return View(reservations);
        }

        [HttpGet]
        public IActionResult GetContact(int id)
        {
            var reservation = _db.Reservations.Include(r => r.User).FirstOrDefault(i => i.Id == id);
            if(reservation == null)
            {
                return NotFound();
            }

            return Json(reservation.User);
        }
    }
}