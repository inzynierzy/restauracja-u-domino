﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Restauracja_u_Domino.Areas.User.ViewModels
{
    public class ReservationsViewModel
    {
        public ReservationsViewModel()
        {
            Reservations = new List<ReservationsViewModel>();
        }

        public List<ReservationsViewModel> Reservations { get; set; }
    }
}
