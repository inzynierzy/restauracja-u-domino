﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Restauracja_u_Domino.DataAccess.Data;
using Restauracja_u_Domino.Models;

namespace Restauracja_u_Domino.Controllers
{
    [Area("User")]
    [Authorize(Roles = "User, Admin, Employee")]
    public class ReservationController : Controller
    {
        private readonly ApplicationDbContext _context;
        private UserManager<ApplicationUser> _userManager;

        public ReservationController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }
        [HttpGet]
        public IActionResult Index()
        {
            ViewBag.UserId = _userManager.GetUserId(User);
            return View();
        }

        [HttpGet]
        public IActionResult UserReservations()
        {
            List<Reservation> Reservations = new List<Reservation>();
            string UserId = _userManager.GetUserId(User);
            Reservations = _context.Reservations.Include(t => t.Table).Where(i => i.UserId == UserId && i.StartDate > DateTime.Now).ToList();
            return View(Reservations);
        }
        public async Task<IActionResult> Delete(int id)
        {
            string UserId = _userManager.GetUserId(User);
            var reservation = await _context.Reservations.FindAsync(id);     
            if(reservation == null)
            {
                return NotFound();
            }
            if(reservation.UserId != UserId)
            {
                return NotFound();
            }
            _context.Reservations.Remove(reservation);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(UserReservations));
        }


        #region API_CALLS
        [HttpPost]
        public IActionResult SearchForTables([FromBody] ReservationSearch search)
        {
            string startDateTime = search.Date + " " + search.StartHour;
            string endDateTime = search.Date + " " + search.EndHour;
            DateTime startDt = DateTime.ParseExact(startDateTime, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
            DateTime endDt = DateTime.ParseExact(endDateTime, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
            if(startDt > endDt)
            {
                return Json(new { error= true, message = "data rozpoczęcia większa niż data zakończenia" });
            }
            var Tables = _context.Reservations.Include(t => t.Table).Where(i => i.StartDate < endDt && startDt < i.EndDate).ToList();
            List<Table> exceptTables = Tables.Select(i => i.Table).ToList();
            List<Table> tables = _context.Tables.ToList();
            if (exceptTables.Count != 0)
            {
                tables = tables.Except(exceptTables).ToList();
            }
            tables = tables.Where(t => t.SeatsNumb == search.SeatsNumber).ToList();
            List<SelectListItem> selectList = new List<SelectListItem>();
            foreach (var table in tables)
            {
                selectList.Add(new SelectListItem()
                { 
                    Text = "Stolik nr " + table.Id + " " + table.Description + ", liczba miejsc: " + table.SeatsNumb,
                    Value = table.Id.ToString()
                });
            }
            return Json(selectList);
        }
        [HttpPost]
        public async Task<IActionResult> MakeReservation([FromBody] ReservationMake reservation)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction(nameof(Index));
            }
            if(reservation == null)
            {
                return Json(new { success = false, message = "Błąd przy składaniu rezerwacji" });
            }
            string startDateTime = reservation.Date + " " + reservation.StartHour;
            string endDateTime = reservation.Date + " " + reservation.EndHour;
            DateTime startDt = DateTime.ParseExact(startDateTime, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
            DateTime endDt = DateTime.ParseExact(endDateTime, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
            Reservation reservationToDb = new Reservation()
            {
                StartDate = startDt,
                EndDate = endDt,
                TableId = reservation.TableId,
                UserId = reservation.UserId
            };
            if(reservation.Comment != "")
            {
                reservationToDb.Comment = reservation.Comment; 
            }
            try
            {
                await _context.Reservations.AddAsync(reservationToDb);
                var success = await _context.SaveChangesAsync();
            }
            catch
            {
                return Json(new { success = false, message = "Błąd przy składaniu rezerwacji" });
            }
            return Json(new { success = true, message = "Dodano rezerwacje" });

        }
        #endregion
    }
    public class ReservationSearch
    {
        public string Date { get; set; }
        public string StartHour { get; set; }
        public string EndHour { get; set; }
        public int SeatsNumber { get; set; }
    }

    public class ReservationMake
    {
        [Required]
        public string Date { get; set; }
        [Required]
        public string StartHour { get; set; }
        [Required]
        public string EndHour { get; set; }
        [Required]
        public int TableId { get; set; }
        public string Comment { get; set; }
        [Required]
        public string UserId { get; set; }
    }

}