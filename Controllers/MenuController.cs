﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Restauracja_u_Domino.DataAccess.Repository.IRepository;
using Restauracja_u_Domino.Models;
using Restauracja_u_Domino.ViewModels;

namespace Restauracja_u_Domino.Controllers
{
    public class MenuController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public MenuController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IActionResult Recommended()
        {
            MenuViewModel menuViewModel = new MenuViewModel();
            var dishes = _unitOfWork.RecommendedDishes.GetAll(includeProperties:"Dish");
            if (dishes.Any())
            {
                menuViewModel.Dishes = dishes.Select(i => i.Dish).ToList();
            }
            return View(menuViewModel);
        }
        public IActionResult Index(int? categoryId)
        {
            MenuViewModel menuViewModel = new MenuViewModel();
            Category category;

            if (categoryId == null)
            {
                category = _unitOfWork.Category.GetFirstOrDefault();
            }
            else
            {
                category = _unitOfWork.Category.GetFirstOrDefault(i => i.Id == categoryId);
            }
            var categories = _unitOfWork.Category.GetAll();
            
            if (categories.Any())
            {
                menuViewModel.Categories = categories.ToList();
            }

            if(category != null)
            {
                menuViewModel.SelectedCategory = category;
                //Add dishes 
                var dishes = _unitOfWork.Dish.GetAll(includeProperties: "Category").Where(i => i.Category == category);
                if (dishes.Any())
                {
                    menuViewModel.Dishes = dishes.ToList();
                }
            }
           
            return View(menuViewModel);
        }
    }
}