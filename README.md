# Restauracja u Domino (Domino's Restaurant)
Web platform for restaurant which allows manage: tables, menu, reservations, employees, users and site. 

## Tech stack
* ASP .NET Core 3.1
* Entity Framework
* MS SQL
* Web Desing Languages (HTML, CSS, JS, JQuery, AJAX)
* IIS
* Bootstrap

<img src="/uploads/50b8861567281848d1d711904eaff5e7/image.png" width="1000" />

## Application contains 4 areas which represents 3 roles and guest

### Guest
* viewing main page with contact info and description
* viewing recommended dishes
* viewing categorized menu
* access to login/register pages
* ability to login with google
<img src="/uploads/403b10f25174061abf406a33eb9220da/menu.PNG" width="800" />

### User
* extends functionality of guest
* making reservations
* managing own reservations
* receiving email notifications about reservation
* manage personal data

<img src="/uploads/688845482a500b9ee20570778bf62c8d/przypomnienied.PNG" width="800" /> </br>
<img src="/uploads/1e35989fcfd41471a2d94c39d7f9d439/rezerwacja.png" width="800" /> </br>
<img src="/uploads/6d3f69f7d6fd11daa416dcbd100b3d7c/rezerwacje.PNG" width="800" /> </br>

### Employee
* extends functionality of user
* viewing all reservations with contact data to user
<img src="/uploads/912f04ceade6ea7189beb3ff232ad02f/podglad_rezerwacji.PNG" width="800" />

### Administrator
* extends functionality of employee
* managing user and roles
* managing tables
* managing categories and menu
* managing recommended dishes

<img src="/uploads/ff6984df827c96aeb2cc224148dcf0b8/manage.PNG" width="800" />
<img src="/uploads/05b6e261486235538418a7178aba2cd4/table.PNG" width="800" />
<img src="/uploads/bfeb723d2a0b729c51aad0e434c99924/role.PNG" width="800" />

## Team
<img src="/uploads/78e6d467043dbd40dab6645b2c786a92/main.PNG" width="1000" />


