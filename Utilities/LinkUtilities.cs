using System.Collections.Generic;
using System.IO;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Routing;

namespace Restauracja_u_Domino.Utilities
{
    public static class LinkUtilities
    {
        public static IHtmlContent ActiveActionLink(this IHtmlHelper html, string linkText, string actionName,
            string controllerName, object routeValues, object htmlAttributes, bool addTagLi)
        {
            return ActiveActionLink(html, linkText, actionName, controllerName, new RouteValueDictionary(routeValues),
                HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes), addTagLi);
        }

        public static IHtmlContent ActiveActionLink(this IHtmlHelper html, string linkText, string actionName,
            string controllerName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes,
            bool addTagLi)
        {
            var routeData = html.ViewContext.RouteData;
            var routeAction = (string) routeData.Values["action"];
            var routeController = (string) routeData.Values["controller"];

            var active = controllerName.Equals(routeController) && actionName.Equals(routeAction);

            using (var writer = new StringWriter())
            {
                if (addTagLi)
                {
                    writer.WriteLine($"<li class='nav-item {(active ? "active" : "")}'>");
                    html.ActionLink(linkText, actionName, controllerName, routeValues, htmlAttributes)
                        .WriteTo(writer, HtmlEncoder.Default);
                    writer.WriteLine("</li>");
                }
                else
                {
                    if (active) htmlAttributes["class"] += " active";

                    html.ActionLink(linkText, actionName, controllerName, routeValues, htmlAttributes)
                        .WriteTo(writer, HtmlEncoder.Default);
                }

                return new HtmlString(writer.ToString());
            }
        }
    }
}