﻿using System;
using System.Threading.Tasks;

namespace Restauracja_u_Domino.Services.EmailSender
{
    public interface IEmailSender
    {
        void SendEmail(Message message);
        Task SendEmailAsync(Message message);
    }
}