﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Restauracja_u_Domino.DataAccess.Data;
using Restauracja_u_Domino.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Restauracja_u_Domino.Services.EmailSender
{
    public class DeleyedEmailSenderService : IHostedService, IDisposable
    {
        private readonly ILogger<DeleyedEmailSenderService> _logger;
        private readonly IServiceProvider _provider;
        private IWebHostEnvironment _hostEnvironment;
        private Timer _timer;

        public DeleyedEmailSenderService(ILogger<DeleyedEmailSenderService> logger, IServiceProvider serviceProvider, IWebHostEnvironment hostEnvironment)
        {
            _provider = serviceProvider;
            _logger = logger;
            _hostEnvironment = hostEnvironment;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Deleyed email sender service running");
            _timer = new Timer(DoWork, null, TimeSpan.Zero,
            TimeSpan.FromMinutes(5));

            return Task.CompletedTask;
        }
        private async void DoWork(object state)
        {
            using (IServiceScope scope = _provider.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
                var _emailSender = scope.ServiceProvider.GetRequiredService<IEmailSender>();
                var reservations = context.Reservations.Include(r => r.User).ToList();
                foreach (Reservation reservation in reservations)
                {
                    if (reservation.StartDate > DateTime.Now)
                    {
                        TimeSpan timeDiff = reservation.StartDate - DateTime.Now;
                        if (reservation.notificationSended == 0)
                        {
                            if (timeDiff.TotalHours <= 24)
                            {
                                if (timeDiff.TotalHours >= 16)
                                {
                                    try
                                    {
                                        string mailText = System.IO.File.ReadAllText(Path.Combine(_hostEnvironment.ContentRootPath, @"Services/EmailSender/EmailTemplates/DayEmailNotif.html"));
                                        mailText = mailText.Replace("[userName]", reservation.User.Name)
                                                  .Replace("[startHour]", reservation.StartDate.ToShortTimeString())
                                                  .Replace("[endHour]", reservation.EndDate.ToShortTimeString());

                                        var message = new Message(new string[] { reservation.User.Email }, "Przypomnienie o rezerwacji", mailText, null);
                                        await _emailSender.SendEmailAsync(message);
                                        _logger.LogInformation("Email Sended");
                                    }
                                    catch
                                    {
                                        _logger.LogError("Cannot find email");
                                    }
                                }
                                reservation.notificationSended++;
                            }
                        }
                        else if (reservation.notificationSended == 1)
                        {
                            if (timeDiff.TotalHours <= 2)
                            {
                                try
                                {
                                    string mailText = System.IO.File.ReadAllText(Path.Combine(_hostEnvironment.ContentRootPath, @"Services/EmailSender/EmailTemplates/2HourEmailNotif.html"));
                                    mailText = mailText.Replace("[userName]", reservation.User.Name)
                                                 .Replace("[startHour]", reservation.StartDate.ToShortTimeString())
                                                 .Replace("[endHour]", reservation.EndDate.ToShortTimeString());
                                    var message = new Message(new string[] { reservation.User.Email }, "Przypomnienie o rezerwacji", mailText, null);
                                    await _emailSender.SendEmailAsync(message);
                                    _logger.LogInformation("Email Sended");
                                }
                                catch
                                {
                                    _logger.LogError("Cannot find email");
                                }
                                reservation.notificationSended++;
                            }
                        }

                    }
                }
                await context.SaveChangesAsync();
            }

            _logger.LogInformation("Checking Mails...");
        }

        public Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Stopping mail service.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
