﻿using Restauracja_u_Domino.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Restauracja_u_Domino.ViewModels
{
    public class MenuViewModel
    {
        public MenuViewModel()
        {
            Dishes = new List<Dish>();
            Categories = new List<Category>();
        }

        public List<Dish> Dishes{ get; set; }
        public List<Category> Categories { get; set; }
        public Category SelectedCategory { get; set; }
    }
}
