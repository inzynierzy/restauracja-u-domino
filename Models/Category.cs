﻿using System.ComponentModel.DataAnnotations;

namespace Restauracja_u_Domino.Models
{
    public class Category
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Nazwa Kategorii")]
        [MaxLength(50)]
        public string CategoryName { get; set; }
    }
}