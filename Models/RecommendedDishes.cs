﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Restauracja_u_Domino.Models
{
    public class RecommendedDishes
    {
        [Key]
        public int Id { get; set; }

        public int DishId { get; set; }
        [ForeignKey("DishId")]
        public Dish Dish { get; set; }
    }
}
