﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Restauracja_u_Domino.Models

{
    public class Dish
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Range(0, 9999)]
        [Display(Name = "Cena")]
        public float Price { get; set; }

        [Required]
        [MaxLength(50)]
        [Display(Name = "Nazwa dania")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Opis dania")]
        public string Description { get; set; }

        [Display(Name = "Adres Obrazka")]
        public string ImageUrl { get; set; }

        [Required]
        [Display(Name = "Kategoria")]
        public int CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        
        public Category Category { get; set; }
    }
}