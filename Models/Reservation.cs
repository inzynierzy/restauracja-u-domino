﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Restauracja_u_Domino.Models
{
    public class Reservation
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public DateTime StartDate{ get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        [Required]
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public ApplicationUser User{ get; set; }
        [Required]
        public int TableId { get; set; }
        [ForeignKey("TableId")]
        public Table Table { get; set; }
        public string Comment { get; set; }
        public int notificationSended { get; set; }

    }
}
