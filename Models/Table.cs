﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Restauracja_u_Domino.Models
{
    public class Table
    {
        [Key]
        [Display(Name = "Numer stolika")]
        public int Id { get; set; }

        [Required]
        [Range(1,10)]
        [Display(Name ="Liczba miejsc")]
        public int SeatsNumb { get; set; }

        [Required]
        [MaxLength(50)]
        [Display(Name = "Opis")]
        public string Description { get; set; }
    }
}
