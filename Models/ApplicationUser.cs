﻿using Microsoft.AspNetCore.Identity;

namespace Restauracja_u_Domino.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string Name { get; set; }

        public string Surname { get; set; }
    }
}