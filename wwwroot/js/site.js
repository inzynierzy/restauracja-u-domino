﻿$(function () {
    const loader = $("#loader");
    const navbar = $(".navbar");
    loader.addClass("loaded");
    loader.attr("src", "~/logo.svg");
    $(window).on('scroll', function () {
        let curScrollPos = $(this).scrollTop();
        curScrollPos > 250 ? navbar.addClass("solid") : navbar.removeClass("solid");
        curScrollPos > 500 ? navbar.addClass("nav-small") : navbar.removeClass("nav-small");
    });
});