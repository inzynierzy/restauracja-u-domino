// sidenav
$(function () {
    $("main").removeClass("dev-margin dev-padding");
    
    ElementsPosition(1);

    $(".sidenav").on("click", "a", function (e) {
        e.preventDefault();
        let _this = parseInt(
            $(this).parent().index() + 1
        );

        ElementsPosition(_this);
        ScrollTo(this.hash);
    });

    $(window).scroll(function () {
        if ($("#about").offset().top < $(window).scrollTop() + $(window).outerHeight()) {
            ElementsPosition(1);
        }
        if ($("#team").offset().top < $(window).scrollTop() + $(window).outerHeight()) {
            ElementsPosition(2);
        }
        if ($("#partners").offset().top < $(window).scrollTop() + $(window).outerHeight()) {
            ElementsPosition(3);
        }
        if ($("#opinions").offset().top < $(window).scrollTop() + $(window).outerHeight()) {
            ElementsPosition(4);
        }
        if ($("#contact").offset().top < $(window).scrollTop() + $(window).outerHeight()) {
            ElementsPosition(5);
        }
    });
});

function ElementsPosition(index) {
    let sidenav_li = $(".sidenav li");
    sidenav_li.removeClass("active");
    sidenav_li.eq(index - 1).addClass("active");

    let gettop = 75 * parseInt(sidenav_li.eq(index - 1).index());
    
    TweenMax.to($(".indicator"), 1, {y: gettop, ease: Power1.easeInOut});
}

function ScrollTo(hash) {
    $('html, body').animate({
        scrollTop: $(hash).offset().top
    }, 800, function () {
        window.location.hash = hash;
    });
}